public class Gorgon extends Creature{

    public Gorgon(String name, int xCoord) {
        super(name, 3, 14, 12, 4, 70, 5, xCoord);
    }

    @Override
    protected void die() {
        super.die();
        System.out.format("%s is speaking hoarsely: 'mooooo...'.%n", this.name);
    }

    @Override
    protected void useSpecial() {
        System.out.format("%s is screaming: 'MOOOOOOOOOOOOOOOOOO'.%nRandom damage of %s is increased by 2.%n", this.name, this.name);
        this.randomDamage += 2;
    }

}

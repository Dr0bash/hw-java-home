public class SerpentFly extends Creature{

    public SerpentFly(String name, int xCoord) {
        super(name, 2, 9, 2, 3, 20, 9, xCoord);
    }

    @Override
    protected void die() {
        super.die();
        System.out.format("%s is speaking hoarsely: 'I guessssssss I'm dead'.%n", this.name);
    }

    @Override
    protected void useSpecial() {
        super.useSpecial();
        if (this.random.nextInt(101) < 10){
            System.out.format("%s is using deadly poison: 'YOU MUST DIE'.%n", this.name);
            currentEnemy.die();
        } else {
            System.out.format("Base damage of %s is increased by 1.%n", this.name);
            this.baseDamage++;
        }

    }


}

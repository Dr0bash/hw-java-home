import java.util.Random;

public abstract class Creature {

    protected Random random = new Random();

    protected String name;

    protected int range;

    protected int defence;

    protected int baseDamage;

    protected int randomDamage;

    protected int health;

    protected int mobility;

    protected Creature currentEnemy = null;

    protected int xCoordinate;

    protected boolean isAlive = true;

    protected Creature(String name, int range, int defence, int baseDamage, int randomDamage, int health, int mobility, int xCoordinate) {
        this.name = name;
        this.range = range;
        this.defence = defence;
        this.baseDamage = baseDamage;
        this.randomDamage = randomDamage;
        this.health = health;
        this.mobility = mobility;
        this.xCoordinate = xCoordinate;
    }

    public void action(Creature[] arrCreature){
        if (this.currentEnemy == null){
            this.chooseEnemy(arrCreature);
        }
        if (Math.abs(this.xCoordinate - this.currentEnemy.xCoordinate) > this.range){
            this.move();
        } else {
            if (this.random.nextInt(51)>25) {
                this.attack();
            } else {
                this.useSpecial();
            }
        }
    }

    protected void attack(){
        int rDamage = this.random.nextInt(this.randomDamage);
        System.out.format("%s attacks %s for ", this.name, this.currentEnemy.name);
        System.out.format("%d damage.%n", this.baseDamage + rDamage);
        currentEnemy.defend(this.baseDamage + rDamage);
    }

    public void defend(int damage){
        if (this.defence > 0) {
            if (this.defence >= damage){
                this.defence -= damage;
                System.out.format("%s blocks %d damage.%n", this.name, damage);
            } else {
                System.out.format("%s blocks %d damage and takes ", this.name, this.defence);
                damage -= this.defence;
                this.defence = 0;
                this.health -= damage;
                System.out.format("%d damage.%n", damage);
            }
        } else {
            this.health -= damage;
            System.out.format("%s takes %d damage.%n", this.name, damage);
        }

        if (this.health <= 0) {
            this.health = 0;
            this.die();
        }

    }

    protected void move(){
        System.out.format("%s moves from %d to ", this.name, this.xCoordinate);
        if (this.xCoordinate > this.currentEnemy.xCoordinate){
            this.xCoordinate -= Math.min(this.xCoordinate - this.range - this.currentEnemy.xCoordinate, this.mobility);
        } else {
            this.xCoordinate += Math.min(this.currentEnemy.xCoordinate - this.range - this.xCoordinate, this.mobility);
        }
        System.out.println(this.xCoordinate + ".");
    }

    protected void chooseEnemy(Creature[] arrCreature){
        for (Creature c : arrCreature){
            if (c != this){
                this.currentEnemy = c;
            }
        }
    }

    protected void die(){
        this.isAlive = false;
    }

    protected void useSpecial(){
        System.out.format("%s is screaming: 'I am your death'.%n", this.name);
    }
}
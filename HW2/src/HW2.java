import java.util.Random;

public class HW2 {

    public static void main(String[] args) {

        Random random = new Random();

        Creature c1 = new SerpentFly("Serpent Fly", 0);
        Creature c2 = new Gorgon("Gorgon", 25);

        Creature[] arrCreature = new Creature[2];
        arrCreature[0] = c1;
        arrCreature[1] = c2;

        if (random.nextInt(51) > 25){
            Creature temp = c2;
            c2 = c1;
            c1 = temp;
        }

        while (c1.isAlive && c2.isAlive){
            c1.action(arrCreature);
            System.out.println();
            if (!c1.isAlive || !c2.isAlive){
                break;
            }
            c2.action(arrCreature);
            System.out.println();
        }

        if (c1.isAlive){
            System.out.println(c1.name  + " win!!!");
        } else {
            System.out.println(c2.name  + " win!!!");
        }

    }
}
public class HW1 {

    public static void main(String[] args) {
        fizzBuzz(45);
        int x = 7;
        System.out.format("%dth Fibonacci's number is %d%n", x, fibonacciRec(x));
        System.out.format("%dth Fibonacci's number is %d%n", x, fibonacciIter(x));
        x = 16;
        int y = 5;
        System.out.format("%d divided by %d is %d%n",x, y, div(x, y));
        System.out.format("Modulus of dividing %d by %d is %d%n", x, y, mod(x, y));
    }

    //x and y are positive
    public static int div(int x, int y){
        int res = 0;
        x = Math.abs(x);
        y = Math.abs(y);
        while (x >= y){
            x -= y;
            ++res;
        }
        return res;
    }

    //x and y are positive
    public static int mod(int x, int y){
        x = Math.abs(x);
        y = Math.abs(y);
        while (x >= y){
            x -= y;
        }
        return x;
    }

    public static void fizzBuzz(int x){
        if (x % 3 == 0){
            System.out.print("fizz");
        }
        if (x % 5 == 0){
            System.out.print("buzz");
        }
        System.out.println();
    }

    public static int fibonacciRec(int x){
        if (x < 1){
            return -1;
        }
        if (x == 1){
            return 0;
        }
        if (x == 2){
            return 1;
        }
        return fibonacciRec(x-1) + fibonacciRec(x-2);
    }

    public static int fibonacciIter(int x){
        if (x < 1){
            return -1;
        }
        if (x == 1){
            return 0;
        }
        if (x == 2){
            return 1;
        }
        int x1 = 0;
        int x2 = 1;
        int i = 2;
        int temp = 0;
        while(i < x){
            ++i;
            temp = x1 + x2;
            x1 = x2;
            x2 = temp;
        }
        return x2;
    }

    //tasks from leetcode.com

    public static boolean isPalindrome(int x) {
        int x1 = x;
        int resx = 0;
        while (x1 > 0){
            resx *= 10;
            resx += x1 % 10;
            x1 = x1 / 10;
        }
        return resx == x;
    }

    public static int[] twoSum(int[] nums, int target) {
        int[] res = new int[2];
        for(int i = 0; i < nums.length-1; ++i){
            for (int j = i+1; j < nums.length; ++j){
                if (nums[i]+nums[j]==target){
                    res[0] = i;
                    res[1] = j;
                    return res;
                }
            }
        }
        return res;
    }

    public static int lengthOfLastWord(String s) {
        int res = 0;
        boolean wasChar = false;
        for (int i = s.length()-1; i >=0; --i) {
            if (Character.isAlphabetic(s.charAt(i))){
                ++res;
                wasChar = true;
            } else if (' ' == s.charAt(i) && wasChar){
                return res;
            }
        }
        return res;
    }

    //x is positive
    public static int mySqrt(int x) {
        long y = 0;
        long yl = 1;
        long yr = x;
        long ans = 0;
        while (yl <= yr){
            y = (yr + yl)/2;
            if (y*y <= x){
                ans = y;
                yl = y+1;
            } else {
                yr = y - 1;
            }
        }
        return (int) ans;
    }

}

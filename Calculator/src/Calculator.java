import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    private static final Pattern BRACKETS = Pattern.compile("\\(.+\\)");
    private static final Pattern DIVMULT = Pattern.compile("-?\\d+(\\.\\d+)?\\s*[*/]\\s*+-?\\d+(\\.\\d+)?");
    private static final Pattern PLUSMINUS = Pattern.compile("-?\\d+(\\.\\d+)?\\s*[\\-+]\\s*+-?\\d+(\\.\\d+)?");

    private static final Pattern ARITHMETIC_EXPRESSION = Pattern.compile("^(\\d+|[-+/*]\\d+|[-+/*]\\(-?\\d+([-+/*]\\d+)*\\))+$");

    public static void main(String[] args) {
        int i = 0;
        String expression = "";
        Scanner scanner = new Scanner(System.in);
        while (i < 3) {
            System.out.print("Введите строку с арифметическим выражением: ");
            expression = scanner.nextLine().replace(" ", "");
            if (checkExpression(expression)){
                break;
            }
            ++i;
            System.out.println("Введённое вами выражение оказалось некорректным.");
        }
        if (i == 3){
            System.out.println("Вы не ввели корректное выражение :(");
            return;
        }
        System.out.println("Результат: " + doMath(expression));
    }

    public static boolean checkExpression(String expression){
        Matcher arithmExpr = ARITHMETIC_EXPRESSION.matcher(expression);
        return arithmExpr.find();
    }

    public static double doMath(String expression){

        Matcher matcherBrackets = BRACKETS.matcher(expression);

        while (matcherBrackets.find()){
            String myGroup = matcherBrackets.group().substring(1, matcherBrackets.group().length()-1);
            expression = expression.replace("(" + myGroup + ")", String.valueOf(doMath(myGroup)));
            matcherBrackets = BRACKETS.matcher(expression);
        }

        Matcher matcherDivMult = DIVMULT.matcher(expression);

        while (matcherDivMult.find()){
            String myGroup = matcherDivMult.group();
            if (myGroup.contains("*")){
                int multIndex = myGroup.indexOf('*');
                expression = expression.replace(myGroup, String.valueOf(doMath(myGroup.substring(0,multIndex))*doMath(myGroup.substring(multIndex+1))));
                matcherDivMult = DIVMULT.matcher(expression);
            } else {
                int divIndex = myGroup.indexOf('/');
                expression = expression.replace(myGroup, String.valueOf(doMath(myGroup.substring(0,divIndex))/doMath(myGroup.substring(divIndex+1))));
                matcherDivMult = DIVMULT.matcher(expression);
            }
        }

        Matcher matcherPlusMinus = PLUSMINUS.matcher(expression);

        while (matcherPlusMinus.find()){
            String myGroup = matcherPlusMinus.group();
            if (myGroup.contains("+")){
                int plusIndex = myGroup.indexOf('+');
                expression = expression.replace(myGroup, String.valueOf(doMath(myGroup.substring(0,plusIndex))+doMath(myGroup.substring(plusIndex+1))));
                matcherPlusMinus = PLUSMINUS.matcher(expression);
            } else {
                int minusIndex = myGroup.indexOf('-');
                expression = expression.replace(myGroup, String.valueOf(doMath(myGroup.substring(0,minusIndex))-doMath(myGroup.substring(minusIndex+1))));
                matcherPlusMinus = PLUSMINUS.matcher(expression);
            }
        }

        return Double.parseDouble(expression);
    }
}
